Feature: Create New Lead

Background: 
Given Open The Browser
And Max the Browser
And Set the TimeOut
And Launch the URL
And Enter the Username as DemoSalesManager
And Enter the Password as crmsfa

Scenario Outline: Positive CreateLead
And Click on the Login Button
And Click CRMSFA
And Click Leads
And Click CreateLead
And Enter Company name as <cname>
And Enter First name as <fname>
And Enter Last name as <lname>
When Click CreateLeadButton
Then Verify firstname as <fname>

Examples:
|cname|fname|lname|
|Capgemini|Martina|F|
|Cognizant|Ishwarya|K|

Scenario: Positive CreateLead
And Click on the Login Button
And Click CRMSFA
And Click Leads
And Click CreateLead
And Enter Company name as Johnson&Controls
And Enter First name as Jenitha
And Enter Last name as S
When Click CreateLeadButton
Then Verify firstname as Jenitha

