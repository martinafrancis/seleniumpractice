package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {
	
	public ChromeDriver driver;
	
	
	@Given("Open The Browser")
	public void openTheBrowser() {
	    System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    driver=new ChromeDriver();
	    
	}

	@Given("Max the Browser")
	public void maxTheBrowser() {
		driver.manage().window().maximize();
	    
	    
	}

	@Given("Set the TimeOut")
	public void setTheTimeOut() {
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	    
	    
	}

	@Given("Launch the URL")
	public void launchTheURL() {
	    
		driver.get("http://leaftaps.com/opentaps/control/main");
	    
	}

	@Given("Enter the Username as (.*)")
	public void enterTheUsernameAsDemoSalesManager(String data) {
	    driver.findElementById("username").sendKeys(data);
	    
	}

	@Given("Enter the Password as (.*)")
	public void enterThePasswordAsCrmsfa(String data) {
		driver.findElementById("password").sendKeys(data);
	    
	}

	@Given("Click on the Login Button")
	public void clickOnTheLoginButton() {
	    driver.findElementByClassName("decorativeSubmit").click();
	    
	}

	@Given("Click CRMSFA")
	public void clickCRMSFA() {
		driver.findElementByLinkText("CRM/SFA").click();
	        
	}

	@Given("Click Leads")
	public void clickLeads() {
		driver.findElementByLinkText("Leads").click();
	     
	    
	}

	@Given("Click CreateLead")
	public void clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	    
	}

	@Given("Enter Company name as (.*)")
	public void enterCompanyNameAsCapgemini(String data) {
	    
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
	    
	    
	}

	@Given("Enter First name as (.*)")
	public void enterFirstNameAsMartina(String data) {
		driver.findElementById("createLeadForm_firstName").sendKeys(data);
	    
	    
	}

	@Given("Enter Last name as (.*)")
	public void enterLastNameAsF(String data) {
		driver.findElementById("createLeadForm_lastName").sendKeys(data);
	    
	    
	}

	@When("Click CreateLeadButton")
	public void clickCreateLeadButton() {
	    driver.findElementByClassName("smallSubmit").click();
	    
	}

	@Then("Verify firstname as (.*)")
	public void verifyFirstname(String data) {
		String name=driver.findElementById("viewLead_firstName_sp").getText();	
		System.out.println(name);
	    if(data.equals(name))
	    {
	    	System.out.println("Name is verified");
	    }
	    else
	    {
	    	System.out.println("Unsuccesfull");
	    }
	}

}
