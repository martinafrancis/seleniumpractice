package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{
	
	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="createLeadForm_companyName") WebElement elecompname;
	@FindBy(id="createLeadForm_firstName") WebElement elefname;
	@FindBy(id="createLeadForm_lastName") WebElement elelname;
	@FindBy(how = How.CLASS_NAME, using = "smallSubmit") WebElement elecreateleadbutton;
	
	public CreateLeadPage enterCompname(String data)
	{
		type(elecompname,data);
		return this;
		
	}
	public CreateLeadPage enterfname(String data)
	{
		type(elefname,data);
		return this;
		
	}
	public CreateLeadPage enterlname(String data)
	{
		type(elelname,data);
		return this;
		
	}
	public ViewLeadPage clickCreateLeadButton()
	{
		click(elecreateleadbutton);
		return new ViewLeadPage();
	}
	
	

}
