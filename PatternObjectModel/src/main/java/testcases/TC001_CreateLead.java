package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDescription = "Create new lead and verify the firstname";
		authors = "martina";
		category = "smoke";
		dataSheetName = "TC001_CreateLead_Positive";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password, String cname, String fname, String lname) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmLink()
		.clickLeads()
		.clickCreateLead()
		.enterfname(fname)
		.enterCompname(cname)
		.enterlname(lname)
		.clickCreateLeadButton()
		.verifyfirstName(fname);
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}

}
