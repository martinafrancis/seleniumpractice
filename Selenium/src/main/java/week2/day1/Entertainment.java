package week2.day1;

public interface Entertainment {
	
	public void switchOn();
	
	public void changeChannel();
	
	public void menuList();
	
	public void increaseVolume();

}
