package week2.day1;

public class SmartTv extends Television{
	
	public void switchOn()
	{
		System.out.println("Switching on smarttv");
	}
	
	public void playGame()
	{
		System.out.println("Playing the games");
	}
	public void connectWifi()
	{
		System.out.println("Connecting to WIFI");
	}
	public void downloadApp()
	{
		System.out.println("Downloading the application");
	}

}
