package week2.day1;

public abstract class Radio {
	
	public void playStation()
	{
		System.out.println("Playing song");
	}
	
    public abstract void changeBattery();
    
}
