package week2.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PrimeNnumbers {

	public static void main(String[] args) {
		System.out.println("Enter the number: ");
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		List<Integer> prime=new ArrayList<>();
		
		for(int i=1; i<=num; i++)
		{
			
			if(i==0||i==1)
			{
				continue;
			}
			else if(i==2)
			{
				prime.add(i);
			}
			else
			{
				int flag=0;
			for(int j=2;j<i;j++)
			{
				if(i%j==0)
				{
					flag=1;
					break;
				}
			}
			if(flag==0)
			{
				prime.add(i);
			}
			
			}
		}
		System.out.println(prime);

	}

}
