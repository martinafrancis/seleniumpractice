package week2.day2;


import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;



public class MaxMob {

	public static void main(String[] args) {
		Map<String,Integer> mobiles=new HashMap<String,Integer>();
		mobiles.put("OPPO", 15000);
		mobiles.put("Apple", 70000);
		mobiles.put("Redmi", 16000);
		mobiles.put("Nokia", 10000);
		int max=0;
		for(Integer mo:mobiles.values())
		{
			if(mo>max)
			{
				max=mo;
			}
		}
		System.out.println(max);
		for(Entry<String, Integer> eachMobile:mobiles.entrySet())
		{
			if(max==eachMobile.getValue())
			{
				System.out.println(eachMobile.getKey()+ "--->"+eachMobile.getValue());
			}
				
		
		}
	}

}
