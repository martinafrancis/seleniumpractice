package week2.day2;

import java.util.HashMap;
import java.util.Map;

public class CharCompany {

	public static void main(String[] args) {
		
		String comp="CAPGEMINI";
		char[] comparr=comp.toCharArray();
		Map<Character,Integer> compname=new HashMap<Character,Integer>();
		for(int i=0; i<comparr.length;i++)
		{
			if(compname.containsKey(comparr[i]))
			{
			    compname.put(comparr[i],compname.get(comparr[i])+1);
			
		}
		else
		{
			compname.put(comparr[i],1);
		}
		
		}
		System.out.println(compname);
	}

}
