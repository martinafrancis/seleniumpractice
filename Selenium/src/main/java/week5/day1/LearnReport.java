package week5.day1;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {

	@Test
	public void createReport() throws IOException {
		
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/results.html");
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		ExtentTest logger=extent.createTest("TC001_CreateLead","Creating the leads");
		logger.assignAuthor("Martina");
		logger.assignCategory("Smoke");
		logger.log(Status.PASS, "The Data DemoSalesManager Entered Successfull", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
		extent.flush();
	}

}
