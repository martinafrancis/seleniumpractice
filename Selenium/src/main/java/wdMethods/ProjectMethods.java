package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

public class ProjectMethods extends SeMethods{
	
	@BeforeMethod(groups= {"any"})
	@Parameters({"url","username","password"})
	public void login(String url,String username,String password)
	{
		beforeMethod();
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement elecrm = locateElement("linktext","CRM/SFA");
		click(elecrm);
	}
	
	@AfterMethod(groups= {"any"})
	public void closeApp() {
		closeBrowser();
	}
	@AfterClass(groups= {"any"})
	public void afterClass() {
		System.out.println("@AfterClass");
	}
	@AfterTest(groups= {"any"})
	public void afterTest() {
		System.out.println("@AfterTest");
	}
	@AfterSuite(groups= {"any"})
	public void afterSuite() {
		endResult();
	}
	
	@BeforeSuite(groups= {"any"})
	public void beforeSuite() {
		startResult();
	}
}
