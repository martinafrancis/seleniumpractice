package wdMethods;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

public class ZoomCarMethods extends SeMethods {
	
	public int maxValue(List<WebElement> p)
	{
		List<Integer> priceval=new ArrayList<>();
		for(WebElement eachprice:p)
		{
			String pr=eachprice.getText();
			int len=pr.length();
			String val=pr.substring(2, len);
			int value=Integer.parseInt(val);
			priceval.add(value);
		}
		int n=priceval.size();
		System.out.println("The today number of results:"+n);
		int value=priceval.get(0);
		for(int i=1;i<n;i++)
		{
			if(value<priceval.get(i))
			{
				value=priceval.get(i);
			}
		}
		return value;
		
	}

}
