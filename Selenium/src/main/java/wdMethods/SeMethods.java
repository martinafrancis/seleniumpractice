package wdMethods;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import utils.Report;

public class SeMethods extends Report implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeOptions op=new ChromeOptions();
				op.addArguments("--disable-notifications");
				driver = new ChromeDriver(op);
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//	System.out.println("The Browser "+browser+" Launched Successfully");
			reportStep("Pass", "The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			System.out.println("The Browser "+browser+" not Launched ");
			reportStep("fail", "The Browser "+browser+" not Launched ");
		} finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);
			case "name": return driver.findElementByName(locValue);
			}
		//	reportStep("Pass", "The element found successfully");
		} catch (NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
		//	reportStep("Pass", "The element is not found located");
		}
		return null;
	}

/*	@Override
	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	} */

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			//System.out.println("The Data "+data+" is Entered Successfully");
		//	reportStep("Pass", "The data "+data+" is Entered Successfully");
		}
		catch(Exception e) {
	//		reportStep("Fail","The data not entered");
		}
		finally {
		takeSnap();
		}
	}

	public void clickWithNoSnap(WebElement ele) {
			ele.click();
			System.out.println("The Element "+ele+" Clicked Successfully");
	}
	@Override
	public void click(WebElement ele) {
	//	try {
		ele.click();
		System.out.println("The Element "+ele+" Clicked Successfully");
	//	reportStep("Pass", "The element "+ele+" clicked Successfully");
	//	}
	//	catch(Exception e)
	//	{
		//	reportStep("Fail","The element not clicked");
	//	}
	//	finally
	//	{
		takeSnap();
	//	}
	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select dd = new Select(ele);
		dd.selectByVisibleText(value);
		System.out.println("The DropDown Is Selected with "+value);
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select dd = new Select(ele);
		dd.selectByIndex(index);
		System.out.println("The DropDown Is Selected with "+index);

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		String title = driver.getTitle();
		
			
		if (title.equals(expectedTitle)) {
			System.out.println("got the expected title"+ title);
			return true;
		}
		else
		{
		return false;
		}
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
String text = ele.getText();
if (text.contentEquals(expectedText)) {
	
	System.out.println("got the expected text"+ text);
}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if (text.contains(expectedText)) {
			System.out.println("Date is verified");
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		
		String exactAtt = ele.getAttribute(value);
		if (attribute.contentEquals(exactAtt)) {
			System.out.println("attributre is getting matched");
			
		}
		

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String partialvalue=ele.getAttribute(value);
		if(attribute.contains(partialvalue))
		{
			System.out.println("Partial Attribute is matched");
		}

	}

	@Override
	public void verifySelected(WebElement ele) {
		
		if(ele.isSelected())
		{
			System.out.println(ele.getText() +"is selected");
		}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		
		if(ele.isDisplayed())
		{
			System.out.println(ele.getText() +"is displayed");
		}
	
	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		driver.switchTo().window(listOfWindow.get(index));
		System.out.println("The Window is Switched ");
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);

	}

	@Override
	public void acceptAlert() {
		Alert promptAlert=driver.switchTo().alert();
		promptAlert.accept();
	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		Alert palert = driver.switchTo().alert();
		String alerttext = palert.getText();
		return alerttext;
		
		
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		driver.quit();
	}


	@Override
	public List<WebElement> locateElements(String locValue) {
		List<WebElement> elements = driver.findElementsByXPath(locValue);
		
		return elements;
		// TODO Auto-generated method stub
		
	}


	@Override
	public int selectDate() {
		Date date=new Date();
		DateFormat sdf=new SimpleDateFormat("dd");
		String today=sdf.format(date);
		int tomorrow=Integer.parseInt(today)+1;
		return tomorrow;
	}

}
