package excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class readExcel {
	
	//@Test
	public static Object[][] createLeadData() throws IOException {
		
		XSSFWorkbook wb=new XSSFWorkbook("./data/createLead.xlsx");
		XSSFSheet sheet = wb.getSheetAt(0);
		int lastRowNum = sheet.getLastRowNum();
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		Object[][] data=new Object[lastRowNum][lastCellNum];
		for(int i=1;i<=lastRowNum;i++)
		{
			XSSFRow row = sheet.getRow(i);
			for(int j=0; j<lastCellNum;j++)
			{
				XSSFCell cell = row.getCell(j);
				
				try {
					String value=cell.getStringCellValue();
					System.out.println(value);
					data[i-1][j]=value;
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					System.out.println(e);
				}
			}
		}
		return data;	
	}
	
public static Object[][] editLeadData() throws IOException {
		
		XSSFWorkbook wb=new XSSFWorkbook("./data/createLead.xlsx");
		XSSFSheet sheet = wb.getSheetAt(1);
		int lastRowNum = sheet.getLastRowNum();
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		Object[][] data=new Object[lastRowNum][lastCellNum];
		for(int i=1;i<=lastRowNum;i++)
		{
			XSSFRow row = sheet.getRow(i);
			for(int j=0; j<lastCellNum;j++)
			{
				XSSFCell cell = row.getCell(j);
				
				try {
					String value=cell.getStringCellValue();
					System.out.println(value);
					data[i-1][j]=value;
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					System.out.println(e);
				}
			}
		}
		return data;	
	}
	
	

}
