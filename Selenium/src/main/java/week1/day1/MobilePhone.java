package week1.day1;

public class MobilePhone {
	
	public void callContact(long mobnumber)
	{
		System.out.println("Calling "+mobnumber);
	}
	
	public void sendSMS(long mobnumber, String sms)
	{
		System.out.println("Sending SMS to "+mobnumber+" "+sms);
	}

}
