package coding.challenge;

import java.util.Scanner;

public class MathOperation {

	public static void main(String[] args) {
		int a,b;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the 1st number: ");
		a=sc.nextInt();
		System.out.println("Enter the 2nd number: ");
		b=sc.nextInt();
		String op;
		System.out.println("Enter the operation: ");
		op=sc.next();
		switch(op)
		{
		case "add":
			System.out.println(a+b);
			break;
		case "sub":
			System.out.println(a-b);
			break;
		case "multiply":
			System.out.println(a*b);
			break;
		case "divide":
			System.out.println(a/b);
			break;
		default: System.out.println("Enter valid operation");
		
		}

	}

}
