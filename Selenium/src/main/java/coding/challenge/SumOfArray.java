package coding.challenge;

import java.util.Scanner;

public class SumOfArray {
	
     public static void main(String[] args)
     {
    	 Scanner sc=new Scanner(System.in);
    	 int size,sum=0;
    	 System.out.println("Enter the array size");
    	 size=sc.nextInt();
    	 int arr[]=new int[size];
    	 for(int i=0; i<size; i++)
    	 {
    		 System.out.println("Enter "+ (i+1) +" value");
    		 arr[i]=sc.nextInt();
    		 sum=sum+arr[i];
 
    	 }
    	 System.out.println(sum);
    	 
     }

}
