package coding.challenge;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class RepeatNoArray {

	public static void main(String[] args) {
		int arr[]= {13,65,15,67,88,65,13,99,67,13,65,87,13};
		ArrayList<Integer> ls=new ArrayList<Integer>();
		for(int i=0; i<arr.length;i++)
		{
			for(int j=i+1;j<arr.length;j++)
			{
				
				if(arr[i]==arr[j])
				{
					
					ls.add(arr[i]);
					break;
				}
				else
				{
					continue;
				}
				
			}
			
		}
		Set<Integer> hs = new HashSet<>();
		hs.addAll(ls);
		System.out.println(hs);

	}

}
