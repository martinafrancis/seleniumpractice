package coding.challenge;

import java.util.Scanner;

public class FizzBuzz {

	public static void main(String[] args) {
		int a,b;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the 1st number: ");
		a=sc.nextInt();
		System.out.println("Enter the 2nd number: ");
		b=sc.nextInt();
		for (int i=a; i<=b; i++)
		{
			if(i%3==0 && i%5==0)
			{
				System.out.println("FIZZBUZZ");
			}
			else if(i%5==0)
			{
				System.out.println("BUZZ");
			}
			else if(i%3==0)
			{
				System.out.println("FIZZ");
			}
			else
			{
				System.out.println(i);
			}
		}
		

	}

}
