package project1;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import wdMethods.ZoomCarMethods;

public class ZoomCar extends ZoomCarMethods {
	
	@Test
	public void zoomCar() throws InterruptedException  {
	
		startApp("Chrome","https://www.zoomcar.com/chennai/");
		click(locateElement("xpath","//a[@class='search']"));
		click(locateElement("xpath","(//div[@class='items'])[2]"));
		click(locateElement("xpath","//button[@class='proceed']"));
		int date=selectDate();
		System.out.println(date);
		WebElement ele=locateElement("xpath","//div[contains(text(),'"+date+"')]");
		click(ele);
		click(locateElement("xpath","//button[@class='proceed']"));
		locateElement("xpath","//div[@class='day picked ']").getText();
		verifyPartialText(locateElement("xpath","//div[@class='day picked ']"),""+date);
		click(locateElement("xpath","//button[@class='proceed']"));
		Thread.sleep(10000);
		List<WebElement> price = locateElements("//div[@class='price']");
		int value=maxValue(price);
		WebElement carname=locateElement("xpath","//div[contains(text(),'"+value+"')]/parent::div/parent::div/preceding-sibling::div[1]/h3");
		System.out.println(carname.getText());
		click(locateElement("xpath","//div[contains(text(),'"+value+"')]/following-sibling::button"));
		closeBrowser();
	}

}
