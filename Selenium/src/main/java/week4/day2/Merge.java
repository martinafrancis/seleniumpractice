package week4.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Merge {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
	//	driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@value='Login']").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> windows=new ArrayList<>();
		windows.addAll(windowHandles);
		driver.switchTo().window(windows.get(1));
		driver.findElementByXPath("(//div[@class='x-form-element'])[2]/input").sendKeys("Martina");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(10000);
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[2]/a").click();
		driver.switchTo().window(windows.get(0));
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		Set<String> windowHandles2 = driver.getWindowHandles();
		List<String> windows2=new ArrayList<>();
		windows2.addAll(windowHandles2);
		driver.switchTo().window(windows2.get(1));
		driver.findElementByXPath("(//div[@class='x-form-element'])[2]/input").sendKeys("Vandana");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(10000);
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[2]/a").click();
		driver.switchTo().window(windows2.get(0));
		driver.findElementByXPath("//a[text()='Merge']").click();
		
		
		Alert promptAlert=driver.switchTo().alert();
		promptAlert.accept();
		

	}

}
