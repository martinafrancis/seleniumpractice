package week4.day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.readExcel;
import wdMethods.ProjectMethods;

public class TC002EditLead extends ProjectMethods{
	
	@BeforeTest(groups= {"any"})
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDesc = "Edit Lead";
		author = "Martina";
		category = "Sanity";
	}
	@DataProvider(name="qa")	
	public Object[][] fetchData() throws IOException{
		
		Object[][] data = readExcel.editLeadData();
		return data;
}
	
	@Test(groups= {"sanity"}, dataProvider="qa")
	
	public void editLead(String fname,String dept)
	{
		WebElement eleleads = locateElement("linktext", "Leads");
		click(eleleads);
		WebElement elefindlead = locateElement("linktext", "Find Leads");
		click(elefindlead); 
		WebElement elefirstname = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(elefirstname, fname);
		WebElement elefindleads = locateElement("xpath", "//button[text()='Find Leads']");
		click(elefindleads);
		WebElement eleselectlead = locateElement("xpath", "(//a[@class = 'linktext'])[4]");
		click(eleselectlead);
		WebElement eleedit = locateElement("xpath", "//a[text() = 'Edit']");
		click(eleedit);
		WebElement eledepartment = locateElement("id", "updateLeadForm_departmentName");
		type(eledepartment, dept);
		WebElement eleupdate = locateElement("xpath", "//input[@value = 'Update']");
		click(eleupdate);
	}

}
