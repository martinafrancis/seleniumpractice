package week4.day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.readExcel;
import wdMethods.ProjectMethods;

public class TC001CreateLead extends ProjectMethods{
	
	@BeforeTest(groups= {"smoke"})
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDesc = "Create A new Lead";
		author = "Martina";
		category = "Sanity";
	}
	
	@DataProvider(name="qa")
	public Object[][] fetchData() throws IOException{
		
		Object[][] data = readExcel.createLeadData();
	/*	Object[][] data=new Object[2][5];
		data[0][0]="TestLeaf";
		data[0][1]="sarath";
		data[0][2]="M";
		data[0][3]="sarath@testleaf.com";
		data[0][4]="123456789";
		
		data[0][0]="TestLeaf";
		data[0][1]="sarath";
		data[0][2]="M";
		data[0][3]="sarath@testleaf.com";
		data[0][4]="123456789"; */
		
		return data;
		
	}
	
	@Test(groups= {"smoke"}, dataProvider="qa")
	public void createLead(String cName,String fName,String lName,String email, String ph) {
		WebElement elecreatelead = locateElement("linktext","Create Lead");
		click(elecreatelead);
		WebElement elecname = locateElement("id","createLeadForm_companyName");
		type(elecname,cName);
		WebElement elefname = locateElement("id","createLeadForm_firstName");
		type(elefname,fName);
		WebElement elelname = locateElement("id","createLeadForm_lastName");
		type(elelname,lName);
		type(locateElement("id","createLeadForm_primaryEmail"),email);
		type(locateElement("id","createLeadForm_primaryPhoneNumber"),ph);		
		WebElement elesub = locateElement("xpath","//input[@type='submit']");
		click(elesub);
		String leadid = getText(locateElement("id","viewLead_companyName_sp"));
		int l=leadid.length();
		String id=leadid.substring(l-6, l-1);
		System.out.println(id);
		
		
	}

}
