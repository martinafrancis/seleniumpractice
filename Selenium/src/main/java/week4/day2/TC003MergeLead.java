package week4.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003MergeLead extends ProjectMethods{
	@BeforeTest(groups= {"any"})
	public void setData() {
		testCaseName = "TC003_MergeLead";
		testDesc = "Merging Lead";
		author = "Martina";
		category = "regression";
	}
	
	//@Test(dependsOnMethods= {"week4.day2.TC001CreateLead.createLead"})
	@Test(groups= {"regression"})
	public void mergeLead() throws InterruptedException 
	{
	//	String parent=driver.getWindowHandle();
		locateElement("linktext","Leads").click();
		locateElement("linktext","Merge Leads").click();
		locateElement("xpath","(//img[@alt='Lookup'])[1]").click();
		switchToWindow(1);
		type(locateElement("xpath","//input[@name='firstName']"),"Martina");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(2000);
		 String text = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[2]/a").getText();
		WebElement ele = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[2]/a");
		clickWithNoSnap(ele);
		switchToWindow(0);
		locateElement("xpath","(//img[@alt='Lookup'])[2]").click();
		switchToWindow(1);
		type(locateElement("xpath","//input[@name='firstName']"),"");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(2000);
		WebElement ele2 = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[2]/a");
		clickWithNoSnap(ele2);
		switchToWindow(0);
		clickWithNoSnap(locateElement("xpath","//a[text()='Merge']"));
		Thread.sleep(30000);
		acceptAlert();
		
	}

}
