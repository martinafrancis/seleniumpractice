package week4.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC005DuplicateLead extends ProjectMethods{

	@BeforeTest()
	public void setData() {
		testCaseName = "TC005_DuplicateLead";
		testDesc = "Duplicate Lead";
		author = "Martina";
		category = "regression";
	}
	
	
	@Test(groups= {"regression"})
	
	public void editLead()
	{
		//login();
		WebElement eleleads = locateElement("linktext", "Leads");
		click(eleleads);
		WebElement elefindlead = locateElement("linktext", "Find Leads");
		click(elefindlead); 
		WebElement elefirstname = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(elefirstname, "Martina");
		WebElement elefindleads = locateElement("xpath", "//button[text()='Find Leads']");
		click(elefindleads);
		WebElement eleselectlead = locateElement("xpath", "(//a[@class = 'linktext'])[4]");
		click(eleselectlead);
		WebElement eleduplicate = locateElement("xpath", "//a[text() = 'Duplicate Lead']");
		click(eleduplicate);
		WebElement eleindustry = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingIndex(eleindustry, 4);
		WebElement elecreatelead = locateElement("xpath", "//input[@value = 'Create Lead']");
		click(elecreatelead);
	}

}
